libhash-asobject-perl (0.13-4) unstable; urgency=medium

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete field Name from debian/upstream/metadata (already present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 12 Dec 2022 20:33:56 +0000

libhash-asobject-perl (0.13-3) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Add upstream/metadata
  * Declare compliance with policy 4.2.1
  * Enable autopkgtest
  * Bump debhelper compat to 10

 -- Xavier Guimard <x.guimard@free.fr>  Mon, 27 Aug 2018 14:58:39 +0200

libhash-asobject-perl (0.13-2) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Add libtest-pod-{,coverage-}perl to Build-Depends-Indep.

  [ Xavier Guimard ]
  * Update source format to 3.0 (quilt)
  * Remove README from docs
  * Bump debian/copyright format to 1.0
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compatibility to 8

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 26 Jan 2013 12:25:54 +0100

libhash-asobject-perl (0.13-1) unstable; urgency=low

  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza). Changed: Maintainer set to Debian Perl Group
    <pkg-perl-maintainers@lists.alioth.debian.org> (was: Ivan Kohler
    <ivan-debian@420.am>); Ivan Kohler <ivan-debian@420.am> moved to
    Uploaders.
  * debian/watch: update to ignore development releases.
  * New upstream release
  * dh-make-perl refresh, dh 7
  * Standards-Version 3.8.4

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 24 Apr 2010 21:49:36 -0700

libhash-asobject-perl (0.10-1) unstable; urgency=low

  * Initial Release. (Closes: #466803)

 -- Ivan Kohler <ivan-debian@420.am>  Wed, 20 Feb 2008 19:15:58 -0800
